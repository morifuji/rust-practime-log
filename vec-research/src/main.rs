fn main() {
    println!("Hello, world!");

    // let primitive_a: i32 = 1;
    // let primitive_b: i32 = primitive_a;
    // println!("{:?}", primitive_a);
    // println!("{:?}", primitive_b);

    let vec_a_1 = vec!(1, 2, 3);
    let vec_a_2 = vec!(1, 2, 3, 4);
    let vec_a_3 = vec!(1, 2, 3, 4, 5);
    let vec_a = vec!(vec_a_1, vec_a_2, vec_a_3);

    // let vec_a_filtered: Vec<&Vec<i32>> = vec_a.iter().filter(|v| {
    //     (*v).len()!=4
    // }).collect();

    let vec_a_filtered_mut: Vec<Vec<i32>> = vec_a.into_iter().filter(|v| {
        (*v).len()!=4
    }).collect();

    println!("{:?}", vec_a);
    println!("{:?}", vec_a_filtered_mut);

    // let vec_b_1= vec!(vec!(1, 2, 3));
    // let vec_b_2 = vec!([1, 2, 3]);

    // let vec_b_1_collected: Vec<&Vec<i32>> = vec_b_1.iter().collect();
    // let vec_b_2_collected: Vec<&[i32;3]> = vec_b_2.iter().collect();

    // let vec_c_1= vec!(1, 2, 3);
    // let vec_c_2 = vec!(1, 2, 3);

    // let vec_c_1_collected: Vec<i32> = vec_c_1.into_iter().collect();
    // let vec_c_2_collected: Vec<&i32> = vec_c_2.iter().collect();


    // let vec_a_filtered: Vec<&i32> = vec_a.iter().filter(|v| **v%2==1).collect();
    // let vec_a_filtered_mut: Vec<Vec<i32>> = vec_a.into_iter().collect();
    // let vec_a_first_mut: Vec<i32> = vec_a_filtered_mut[0].into_iter().collect();
    // println!("{:?}", vec_a_first_mut);

    // println!("{:?}", vec_a_filtered_mut);

    // let vec_a_first: Vec<&i32> = vec_a_filtered_mut[0].iter().collect();
    // println!("{:?}", vec_a_first);

    // println!("{:?}", vec_a_filtered_mut);

    // let vec_a_first_mut: Vec<i32> = vec_a_filtered_mut[0].into_iter().collect();
    // println!("{:?}", vec_a_first_mut);


    // let vec_a_filtered_mut: Vec<Vec<i32>> = vec_a.into_iter().filter(|v| {
    //     (*v).len()==3
    // }).collect();
    // print!("{:?}", vec_a_filtered_mut);

    // let vec_a_filtered2: Vec<&i32> = vec_a.iter().filter(|v| **v%2==0).collect();
    // let vec_a_filtered3: Vec<&i32> = vec_a.iter().filter(|v| **v%2==0).collect();
    // let vec_b_filtered: Vec<i32> = vec_a.into_iter().filter(|v| v%2==0).collect();

    // print!("{:?}", vec_a);
    // print!("{:?}", vec_a_filtered);
    // print!("{:?}", vec_a_filtered2);
    // print!("{:?}", vec_a_filtered3);
    // print!("{:?}", vec_b_filtered);
}
