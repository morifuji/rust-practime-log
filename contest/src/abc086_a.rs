fn main() {
    let numbers = read();
    let iter: Vec<&str> = numbers.split_whitespace().collect();

    let a:i32 = iter[0].parse().unwrap();
    let b:i32  = iter[1].parse().unwrap();

    println!("{}", if ((a * b)%2 == 0) {
        "Even"
    } else {
        "Odd"
    });
}

fn read() -> String {
    let mut s = String::new();
    std::io::stdin().read_line(&mut s);
    s.to_string()
}

fn readAndCast<T: std::str::FromStr>() -> T {
    let mut s = String::new();
    std::io::stdin().read_line(&mut s);
    println!("{}", s);
    s.trim().parse().ok().unwrap()
}

