
fn main() {
    let a: i32 = read().trim().parse().unwrap();
    let b: i32 = read().trim().parse().unwrap();
    let c: i32 = read().trim().parse().unwrap();
    let x: i32 = read().trim().parse().unwrap();

    let mut patternNum = 0;
    'a: for aNum in 0..a+1 {
        if (aNum * 500) > x {
            continue 'a;
        }

        'b: for bNum in 0..b+1 {

            if (aNum * 500 + bNum * 100) > x {
                continue 'b;
            }
            
            for cNum in 0..c+1 {
                let sum = (aNum * 500 + bNum * 100 + cNum * 50);
                if sum == x {
                    patternNum = patternNum + 1;
                }
            }   
        }   
    }

    println!("{}", patternNum);
}

fn read() -> String {
    let mut s = String::new();
    std::io::stdin().read_line(&mut s);
    s.to_string()
}
