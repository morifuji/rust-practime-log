
fn dig(currentChars: &Vec<char>, nChars: &Vec<char>, currentPoint: u32, a: u32, b: u32, isMax: bool) -> u32{
    if nChars.len() == 0 {
        if a > currentPoint || currentPoint > b {
            return 0;
        }
        let pointString: String = currentChars.iter().cloned().collect::<String>();
        let pointNum: u32 = pointString.parse().unwrap();
        return pointNum;
    }

    let mut choisebleNumber;
    let mut lastChars;
    let mut firstNumber;
    let mut tempChars = nChars.clone();
    if isMax {
        lastChars = tempChars.split_off(1);
        firstNumber = tempChars[0].to_digit(10).expect("失敗した？？");
        choisebleNumber = 0..firstNumber+1;
    } else {
        choisebleNumber = 0..10;
        lastChars = tempChars.split_off(1);
        firstNumber = 0;
    }

    let mut pointSum = 0;
    for i in choisebleNumber {
        let addedPoint = currentPoint + i;

        // 多すぎたらまずは足切り
        if addedPoint>b {
            break;
        }

        let _isMax = isMax && firstNumber == i;

        let mut _currentChars = currentChars.clone();

        let mut iChar = [std::char::from_digit(i, 10).unwrap()].to_vec();
        _currentChars.append(&mut iChar);

        pointSum = pointSum + dig(&mut _currentChars, &lastChars, addedPoint, a, b, _isMax);
    }

    return pointSum;
}

fn main() {
    let inputs: Vec<u32> = read().trim().split_whitespace().map(|x| x.parse().ok().unwrap()).collect();
    let n: u32 = inputs[0];
    let a = inputs[1];
    let b = inputs[2];

    let mut nChars = n.to_string().chars().collect();

    let empty: Vec<char> = "".chars().collect();

    let point = dig(&empty, &nChars, 0, a, b, true);
    println!("{}", point);
}

fn read() -> String {
    let mut s = String::new();
    std::io::stdin().read_line(&mut s);
    s.to_string()
}
