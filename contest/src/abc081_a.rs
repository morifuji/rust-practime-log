fn main() {
    let numbers: Vec<char> = read().chars().collect();

    let mut count = 0;
    for n in 0..3 {
        if numbers[n] == '1' {
            count = count + 1;
        }
    }

    println!("{}", count);
}

fn read() -> String {
    let mut s = String::new();
    std::io::stdin().read_line(&mut s);
    s.to_string()
}
