use std::collections::HashSet;

fn main() {
    let num = read().trim().parse().unwrap();
    let mut a: HashSet<u32> = vec!().into_iter().collect();
     
    for i in 0..num {
        a.insert(read().trim().parse().unwrap());
    }

    println!("{}", a.len());
}

fn read() -> String {
    let mut s = String::new();
    std::io::stdin().read_line(&mut s);
    s
}
