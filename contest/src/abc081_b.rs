
fn main() {
    let mut dividingNumbers = (1..27).rev().map(|x:i32| 2_i32.pow(x as u32)).enumerate();
    // println!("{:?}", dividingNumbers);

    let mut inputs: Vec<u32> = read().trim().split_whitespace().map(|x| x.parse::<u32>().unwrap()).collect();
    // println!("{:?}", inputs);

    let mut count = 0;
    for (index, dividingNum) in dividingNumbers {
        // println!("{}", inputs.iter().all(|x| x%(dividingNum as u32)==0));
        if inputs.iter().all(|x| x%(dividingNum as u32)==0) {
            count = count + (26 - index);
            inputs = inputs.iter().map(|x| x/(dividingNum as u32)).collect();
        }
    }

    println!("{}", count);
}

fn read() -> String {
    let mut s = String::new();
    // 最初は無視
    std::io::stdin().read_line(&mut String::new());

    std::io::stdin().read_line(&mut s);
    s.to_string()
}
