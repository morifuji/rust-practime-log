fn main() {
    let mut inputs: Vec<i32> = read().trim().split_whitespace().map(|x| x.parse().unwrap()).collect();
    inputs.sort();

    let oddSum: i32 = inputs.iter().enumerate().filter(|&(i, _)| i%2==0).map(|(_, e)| e).sum();
    let evenSum: i32 = inputs.iter().enumerate().filter(|&(i, _)| i%2==1).map(|(_, e)| e).sum();
    
    println!("{}", (oddSum-evenSum).abs());
}

fn read() -> String {
    let mut s = String::new();

    // 最初は無視
    std::io::stdin().read_line(&mut String::new());

    std::io::stdin().read_line(&mut s);
    s
}
