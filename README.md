
# 概要
rustを勉強した際の記録です


# ディレクトリ

```
 $ tree -L 1
.
├── README.md
├── app                 # たたき台サンプル
├── contest             # Atcoder精選10門 https://qiita.com/drken/items/fd4e5e3630d0f5859067
├── docker-compose.yml  # VScodeのRemote Development用
├── grep                # The bookの12章の成果物 https://doc.rust-lang.org/book/
└── server              # The bookの20章の成果物 https://doc.rust-lang.org/book/

```