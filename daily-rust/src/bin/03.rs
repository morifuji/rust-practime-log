extern crate csv;
extern crate rustc_serialize;

use csv::Writer;

#[derive(RustcDecodable, RustcEncodable)]
struct Movie {
    title: String,
    bad_guy: String,
    pub_year: usize,
}

fn main() {
    let dollar_films = vec![
        ("A Fistful of Dollars", "Rojo", 1964),
        ("For a Few Dollars More", "El Indio", 1965),
        ("The Good, the Bad and the Ugly", "Tuco", 1966),
    ];

    let path = "weston.csv";
    let mut writer = Writer::from_path(path).unwrap();

    for row in dollar_films {
        writer.serialize(row).unwrap();
    }

    // encodeエラー
    // error[E0599]: no method named `encode` found for type `csv::Writer<std::fs::File>` in the current scope
    
    // let movie = Movie {
    //     title: "Hang 'Em High".to_string(),
    //     bad_guy: "Wilson".to_string(),
    //     pub_year: 1968,
    // };

    // writer.serialize(movie).expect("CSV writer error");
    // writter.flush().expect("Flush error");
}