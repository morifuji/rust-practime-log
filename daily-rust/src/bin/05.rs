extern crate hyper;
extern crate tokio;

use hyper::{Client, Uri};

#[tokio::main]
async fn main() {
    let client = Client::new();

    let url:Uri = "http://httpbin.org/status/201".parse().expect("invalid URL");

    let mut response = match client.get(url).await {
        Ok(response) => response,
        Err(_) => panic!("Whoops.")
    };

    println!("header is {}", response.status());
    println!("body is {:?}", response.body());
}