use std::env;
use std::process;

extern crate grep;
use grep::Config;

fn main() {
    let args: Vec<String> = env::args().collect();

    let config = Config::new(&args).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    // {}を探しています
    println!("Searching for {}", config.query);
    // {}というファイルの中
    println!("In file {}", config.filename);

    // run(config);

    if let Err(e) = grep::run(config) {
        eprintln!("Application error: {}", e);

        process::exit(1);
    }

    // let mut f = File::open(config.filename).expect("file not found");

    // let mut contents = String::new();

    // // // println!("{}", contents);
    // // let mut hoge = &mut contents;
    // // hoge.push_str(" world");
    // // println!("With text:\n{}", hoge);
    // // let mut zzzz = &mut contents;

    // // let mut huga = &mut contents;
    // // huga.push_str(" 2222");
    // // println!("With text:\n{}", huga);

    // f.read_to_string(&mut contents).expect("something went wrong reading the file");

    // println!("With text:\n{}", contents);   
}



// fn parse_config(args: &[String]) -> Config{
//     let query = args[1].clone();
//     let filename = args[2].clone();

//     Config {query, filename}
// }
