/**
 * 1万回呼び出しした際の、パフォーマンスを `Arc<Mutex>`と`Rc<RefCell>`の場合で比較
 **/
 extern crate ws;
 use ws::{connect, CloseCode, Sender};

 fn main() {
     connect("ws://127.0.0.1:3012", |out| {
        for i in 0..5 {
            println!("{}", i);
        }
 
         move |msg: ws::Message| {
             Ok(())
         }
     });
 }
 
 