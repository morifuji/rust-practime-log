extern crate ws;

use std::rc::Rc;
use std::cell::Cell;

use std::sync::Arc;

use std::sync::Mutex;
use std::fmt::Debug;
use ws::{listen, Handler, Sender, Message, Handshake, CloseCode, Error};
use rand::Rng;
use serde_json::{Result, Value};
use serde::{Deserialize, Serialize};

use std::marker::Copy;
use std::cell::RefCell;
use std::collections::HashSet;

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct MessageFromClient {
    #[serde(default)]
    pub _type: String,
    #[serde(default)]
    pub user_id: u32,
    #[serde(default)]
    pub message: String,
}

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct MessageFromServer {
    #[serde(default)]
    pub _type: String,
    #[serde(default)]
    pub user_id: u32,
    #[serde(default)]
    pub message: String,
}
