extern crate ws;

use ws::{connect, CloseCode, Sender};

use serde_json::{Result, Value};
use serde::{Deserialize, Serialize};

// #[derive(Serialize, Deserialize, Debug)]
// struct MessageFromClient {
//     // _type: i32,
//     _type: String,
// }

// #[derive(Serialize, Deserialize, Debug)]
// struct MessageFromServer {
//     // _type: i32,
//     _type: String,
//     token: usize,
// }

// #[derive(Debug, Default)]
// struct Me {
//     out: Sender,
//     user_id: u32,
// }

mod model;

fn main() {
    // let me: Option<Me> = None;

    let aa = connect("ws://127.0.0.1:3012", |out| {

        out.send("helloooooooooo!");


        out.send("Are you ok?");

        // // 準備完了
        // println!("token is {:?}", out.token());
        // println!("sending..... {:?}", out);

        // let message = MessageFromClient {
        //     _type: "hello".to_string()
        // };
        // let message_str = serde_json::to_string(&message).unwrap();
        // out.send(message_str);

        // let re_message = MessageFromClient {
        //     _type: "hello".to_string()
        // };
        // let re_message_str = serde_json::to_string(&message).unwrap();
        // out.send(re_message_str);

        // // out.send("aaaa").unwrap();
        // // println!("send!");
        // // println!("receiving.....");
        // // move |msg: ws::Message| {
        // //     let message_srt = msg.into_text().unwrap();
        // //     let message_from_server: MessageFromServer = serde_json::from_str(&message_srt).unwrap();
        // //     match &message_from_server._type[..] {
        // //         "hello" => println!("hello response!!!! your token is {:?}", message_from_server.token),
        // //         _ => println!("other message come. it is {:?}", message_from_server)
        // //     };
        // //     Ok(())
        // // }

        // move |msg: ws::Message| {
        //     // println!("me {:?}", me);
        //     let msg_srt = msg.into_text().unwrap();
        //     let message: model::MessageFromServer = serde_json::from_str(&msg_srt).unwrap();
        //     println!("Got message {:?}", message);

        //     let borrowed_out = out;
        //     hello(borrowed_out, message);

        //     // match &message._type[..] {
        //     //     "hello" => hello(borrowed_out, message),
        //     //     _ => println!("?????")
        //     // };
        //     Ok(())
        // }

        // let userid: Option<u32> = None;



        // let a = 1;

        // // The handler needs to take ownership of out, so we use move
        move |msg: ws::Message| {

            // let msg_srt = msg.into_text().unwrap();
            // let message: model::MessageFromServer = serde_json::from_str(&msg_srt).unwrap();
            println!("Got message {:?}", msg.into_text());

            // println!("{}", a);

            // // a = 1;

            // match &message._type[..] {
            //     "hello" => hello(&out, message),
            //     _ => println!("???????")
            // }

            Ok(())
        }
    });
}


fn hello (out: &Sender, message: model::MessageFromServer) {
    // *userid = Some(message.user_id);

    let message_to = model::MessageFromClient {
        _type: "hogehoge".to_string(),
        user_id: message.user_id,
        message: "helllllllloooo".to_string()
    };
    out.send(serde_json::to_string(&message_to).unwrap());
}

// fn hello(out: &Sender, message: model::MessageFromServer) {
//     // let message = model::MessageFromClient {
//     //     _type: "hello",
//     //     user_id: 2,
//     //     message: ""
//     // };
//     // out.send()
// }

