#[derive(Debug)]
struct A {
    id: u32,
    classmates: Vec<A>
}

impl A {
    fn add_friends(self) {
        // これを外すとエラー。
        // self.classmates.push(self);
    }
}

fn main() {
    let yamada = A {
        id: 1,
        classmates: vec!()
    };

    yamada.add_friends();
}