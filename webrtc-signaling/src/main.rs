extern crate ws;

use std::rc::Rc;
use std::cell::Cell;

use std::sync::Arc;

use std::sync::Mutex;
use std::fmt::Debug;
use ws::{listen, Handler, Sender, Message, Handshake, CloseCode, Error};
use rand::Rng;
use serde_json::{Result, Value};
use serde::{Deserialize, Serialize};

use std::marker::Copy;
use std::cell::RefCell;
use std::collections::HashSet;

type Users = Arc<Mutex<Vec<Client>>>;
// type Users = Rc<RefCell<Vec<UserCopiable>>>;

type UserIds = Rc<RefCell<Vec<u32>>>;


// mod model;
// // pub use ./src/model;


// #[derive(Debug, Clone)]
// struct User {
//     out: Sender,
//     user_id: u32,
//     users: Users,
//     userids: UserIds,
// }

// #[derive(Clone, Debug)]
// struct UserCopiable {
//     out: Sender,
//     user_id: u32
// }

// impl Handler for User {


//     fn on_open<'a>(&'a mut self, _: Handshake) -> ws::Result<()> {
    
//         let message = model::MessageFromServer {
//             _type: "hello".to_string(),
//             user_id: self.user_id,
//             ..Default::default()
//         };
//         self.out.send(serde_json::to_string(&message).unwrap());

//         // let mut users = self.users.borrow_mut();
//         // users.push(self);

//         let mut userids = self.userids.borrow_mut();
//         let id = self.user_id;
//         userids.push(id);

    
//         self.users.borrow_mut().iter().for_each(|user|{
//             println!("userid is {:?}", user.user_id);
//         });
//         // self.users.borrow_mut().push(User.clone())
//         Ok(())
//     }

//     fn on_message(&mut self, msg: Message) -> ws::Result<()> {

//         // let message: model::MessageFromClient = serde_json::from_str(&msg.into_text().unwrap()).unwrap();
//         // println!("{:?}", message);

//         // self.out.broadcast(serde_json::to_string(&message).unwrap());


//         let message:Value = serde_json::from_str(&msg.into_text().unwrap()).unwrap();
            
//         println!("data is : {:?}", message);
//         self.out.broadcast(serde_json::to_string(&message).unwrap());

//         // 既に登録済みのユーザーかどうかチェック
//         // let user = self.users.borrow_mut().len();
//         // let mut user_list: Vec<User> = &a;
//         // println!("list is {:?}", a);
//         // let is_joined = user_list.iter_mut().any(|x| {
//         //     println!("checking... {:?}", x.user_id);
//         //     x.user_id == message.user_id!
//         // });

//         // if !is_joined {
//         //     println!("invalid access!!!! {:?}", message.user_id);
//         //     return Ok(())
//         // }        

//         // let token: usize = usize::from(self.out.token());

//         // match &message._type[..] {
//         //     "hello" => {
//         //         let message_to__client = MessageFromServer {
//         //             _type: "hello".to_string(),
//         //             user_id: token
//         //         };
//         //         self.out.send(serde_json::to_string(&message_to__client).unwrap());
//         //         // self.out.send("eengwaogewa");
//         //     },
//         //     _ => println!("###")
//         // }

//         // println!("this id is {:?}", self.out.token());

//         // let message_to__client = MessageFromServer {
//         //     _type: "fuck!".to_string(),
//         //     user_id: token,
//         // };
//         // self.out.send(serde_json::to_string(&message_to__client).unwrap())

//         // let members = self.members.lock().unwrap();

//         // members.iter().for_each(|user| {
//         //     (*user).out.send("Are you happy??");
//         // });

//         // println!("current member is {:?}", self.members.lock().unwrap());

//         // Echo the message back
//         // self.out.broadcast(msg)


//         Ok(())
//     }

//     fn on_close(&mut self, code: CloseCode, reason: &str) {
//         match code {
//             CloseCode::Normal => println!("The client is done with the connection."),
//             CloseCode::Away   => println!("The client is leaving the site."),
//             CloseCode::Abnormal => println!(
//                 "Closing handshake failed! Unable to obtain closing status from client."),
//             _ => println!("The client encountered an error: {}", reason),
//         }

//         // // The connection is going down, so we need to decrement the count
//         // self.count.set(self.count.get() - 1)
//     }

//     fn on_error(&mut self, err: Error) {
//         println!("The server encountered an error: {:?}", err);
//     }
// }

// fn main() {
//   // Cell gives us interior mutability so we can increment
//   // or decrement the count between handlers.
//   // Rc is a reference-counted box for sharing the count between handlers
// //   // since each handler needs to own its contents.
// //   let members: Arc<Mutex<Vec<User>>> = Arc::new(Mutex::new(vec!()));

// //   let members_ref: Rc<RefCell<Vec<USer>>> = Rc::new(RefCell::new(vec!()));
// //   let members_mutex: Rc<Mutex<Vec<Server>>> = Rc::new(Mutex::new(vec!()));

// //   let members_mutex_cell: Rc<Mutex<Vec<RefCell<Server>>>> = Rc::new(Mutex::new(vec!()));

// //   let members_arc_mutex: Arc<Mutex<Vec<Server>>> = Arc::new(Mutex::new(vec!()));

// //   let members_arc_refcell: Arc<RefCell<Vec<Server>>> = Arc::new(RefCell::new(vec!()));

// // //   let senders: Rc<Mutex<Vec<Server>>> = Rc::new(Mutex::new(vec!()));

//     //   let sender_list: Rc<Mutex<Vec<Server>>> = Rc::new(Mutex::new(vec!()));

//     // let members_box = Box::new(vec!());
// let users = Users::new(RefCell::new(vec!()));
// let userids = UserIds::new(RefCell::new(vec!()));

//   listen("127.0.0.1:3012", move |out| { 


//     // let mut users = self.users.borrow_mut();
//     // users.push(self);

//     let mut rng = rand::thread_rng();
//     let uuid = rng.gen();
//     let user = User { 
//       out: out.clone(),
//       user_id: uuid,
//       users: users.clone(),
//       userids: userids.clone()
//     //   members: members.clone(),
//     //   members_ref: members_ref.clone(),
//     // //   members_mutex: members_mutex.clone(),
//     // //   members_mutex_cell: members_mutex_cell.clone(),
//     // //   members_arc_mutex: members_arc_mutex.clone()
//     // // members_box: members_box
//     //     members_arc_refcell: members_arc_refcell.clone()
//     };

//     // let user_copiable = UserCopiable {
//     //     out: out.clone(),
//     //     user_id: uuid,
//     // };

//     users.borrow_mut().push(user.clone());

//     //   senders: senders

//     // let members_ref_local = members_ref.clone();
//     // // members_ref_local.borrow_mut().push(server);

//     // let members_mutex_local = members_mutex.clone();
//     // // members_mutex_local.lock().unwrap().push(server);

//     // let members_arc_refcell_local = members_arc_refcell.clone();

//     // let server_rc = Arc::new(server);


//     // {
//     //     let members_local = members.clone();
//     //     let members_local_locked = members_local.lock().unwrap();

//     //     let server_2 = server_rc.borrow_mut();

//     //     members_local.push(Arc::clone(*server_2));
//     // }
    

//     // members_arc_refcell_local.borrow_mut().push(server);

//     // Rc::new(RefCell::new(&server));

//     // let server_arc_mutex = Arc::new(Mutex::new(server));

//     // let server_cloned = Arc::clone(&server_arc_mutex);

//     // let aa = server_cloned.lock().unwrap();
//     // let bb = aa.clone();
    
//     // // hoge.push(server_cloned);
//     // let server_cloned_2 = Arc::clone(&server_arc_mutex);

//     // let server_cloned = Arc::new(RefCell::new(&server));

//     // let server_cloned_1 = Arc::clone(&server_cloned);
//     // let server_cloned_2 = Arc::clone(&server_cloned);

//     // let members_local = members.clone();
//     // let members_local_locked = members_local.lock().unwrap();

//     // members_local_locked.push(&server_cloned_1.borrow_mut());

//     // println!("current member is {:?}", members_local_locked);

    
//     // server_cloned_2.lock().unwrap().clone()

//     user
// }).unwrap()
// } 

#[derive(Clone, Debug)]
struct Client {
    out: Sender,
    user_id: u32,
    // client_list: Rc<RefCell<Vec<Client>>>,
    client_list: Arc<Mutex<Vec<Client>>>,
}

impl Handler for Client {
    fn on_message(&mut self, msg: Message) -> ws::Result<()> {
        self.client_list.lock().unwrap().iter().filter(|user| user.user_id != self.user_id)
        .for_each(|user|{
             user.out.send(msg.clone());
        });
        Ok(())
    }

    fn on_open<'a>(&'a mut self, _: Handshake) -> ws::Result<()> {
      println!("current user is below.");
      self.client_list.lock().unwrap().iter().for_each(|client|{
          print!("{:?}, ", client.user_id);
      });
      println!("");

      Ok(())
    }
}

fn main() {
  let client_list = Arc::new(Mutex::new(vec!()));

  listen("127.0.0.1:3012", move |out| { 
    let client = Client { 
      out: out.clone(),
      user_id: rand::thread_rng().gen(),
      client_list: client_list.clone(),
    };
    client_list.lock().unwrap().push(client.clone());

    client
  }).unwrap()
} 