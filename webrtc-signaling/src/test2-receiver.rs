/**
 * 1万回呼び出しした際の、パフォーマンスを `Arc<Mutex>`と`Rc<RefCell>`の場合で比較
 **/
 extern crate ws;

 use ws::{connect, CloseCode, Sender};
 
 use serde_json::{Result, Value};
 use serde::{Deserialize, Serialize};
 
 use std::time::{SystemTime, UNIX_EPOCH};


 mod model;
 
 fn main() {
     let aa = connect("ws://127.0.0.1:3012", |out| {
 
         move |msg: ws::Message| {

            let message = msg.into_text().unwrap();

            if message == "0" {
                println!("start at {:?}", SystemTime::now().duration_since(UNIX_EPOCH));
            }

            if message == "9999" {
                println!("start at {:?}", SystemTime::now().duration_since(UNIX_EPOCH));
                std::process::exit(0);
            }
             Ok(())
         }
     });
 }
 
 