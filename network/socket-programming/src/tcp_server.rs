use std::io::{Read, Write};
use std::net::{TcpListener, TcpStream};
use std::{str, thread};


/**
 * 指定のソケットアドレスで接続を待ち受ける
 */
pub fn serve(address: &str) -> Result<(), failure::Error> {
    // ? について
    // https://qiita.com/kanna/items/a0c10a0563573d5b2ed0
    let listner = TcpListener::bind(address)?;
    loop {
        let (stream, _) = listner.accept()?;   
        // moveについて
        // https://doc.rust-jp.rs/the-rust-programming-language-ja/1.6/book/closures.html#move-%E3%82%AF%E3%83%AD%E3%83%BC%E3%82%B8%E3%83%A3
        thread::spawn(move || {
            handler(stream).unwrap_or_else(|error| error!("{:?}", error));
        });
    }
}


fn handler(mut stream: TcpStream) -> Result<(), failure::Error> {
    debug!("Handling data from {}", stream.peer_addr()?);
    let mut buffer = [0u8; 1024];
    loop {
        let nbytes = stream.read(&mut buffer)?;
        if nbytes == 0 {
            debug!("Connection closed.");
            return Ok(());
        }
        print!("{}", str::from_utf8(&buffer[..nbytes])?);
        // print!("size is {}", nbytes);
        // stream.write_all(&buffer[..nbytes])?;
        // stream.write_all(b"HTTP/1.1 200 OK\r\n\r\n")?;
        stream.write_all("HTTP/1.1 200 OK\r\n\r\nHellp World".as_bytes())?;
        stream.flush().unwrap();
        // stream.shutdown(Shutdown::Both);
    }
}