// Elasticsearch
#[macro_use]
extern crate elastic_derive;
extern crate serde_json;
extern crate elastic;

// HTTPリクエスト
use elastic::prelude::*;
use elastic::prelude::{ Date };
use elastic::client::responses::IndexResponse;
use serde::{Serialize, Deserialize};
use std::time::Duration;
extern crate reqwest;

// XML
use quick_xml::Reader;
use quick_xml::events::Event;

// 日付
use chrono::DateTime;
// use elastic::types::date::prelude::Date;

// Future
// use std::future::Future;
use futures::future::Future; // Note: It's not `futures_preview`

#[derive(ElasticType, Serialize, Deserialize, Default, Debug, Clone)]
struct LawSummary {
	pub name: String,
    pub no: String,
    pub date: Date<DefaultDateMapping>,
}

#[derive(ElasticType, Serialize, Deserialize, Default, Debug, Clone)]
struct LawDetail {
    pub name: String,
    pub no: String,
    pub date: Date<DefaultDateMapping>,
    pub content: String
}

fn main() {
    let xml_content = get_xml_content();
    let law_summary_list = xml_to_entity(xml_content.unwrap());

    println!("size is {}", law_summary_list.len());

    let client = reqwest::Client::builder()
    .timeout(Duration::from_secs(90))
    .build().unwrap();

    let law_detail_list = law_summary_list.into_iter().map(|law| {

        let content_list = get_content_by_no(&law.no, &client);

        println!("no {} has loaded.", law.no);

        LawDetail {
            content: content_list.join("\n"),
            name: law.name,
            no: law.no,
            date: law.date
        }
    }).collect();

    println!("all data is ready");

    save(law_detail_list);
}

fn xml_to_entity(xml: String) -> Vec<LawSummary>{
    let mut law_list = vec!();

    let mut reader = Reader::from_str(&xml);
    reader.trim_text(true);

    let mut buf = Vec::new();

    let mut temp_law: LawSummary = Default::default();

    // The `Reader` does not implement `Iterator` because it outputs borrowed data (`Cow`s)
    let mut current_tag_name: Option<String> = None;
    loop {
        match reader.read_event(&mut buf) {
            Ok(Event::Start(ref e)) => {
                current_tag_name = Some(String::from_utf8(e.name().to_vec()).unwrap());
                // if e.name() == b"LawNameListInfo" {
                //     current_tag_name = Some(String::from_utf8(e.name().to_vec()).unwrap());
                //     println!("{:?}", current_tag_name);
                // }
            },
            Ok(Event::End(e)) => {
                current_tag_name = None;
                if e.name() == b"LawNameListInfo" {
                    // listに追加
                    // println!("{:?}", temp_law);
                    law_list.push(temp_law.clone());
                }  
            },
            Ok(Event::Text(e)) => {
                if current_tag_name.as_ref() == None {
                    continue;
                }

                match current_tag_name.as_ref().unwrap().as_str() {
                    // "LawName" => temp_law.name = e.unescape_and_decode(&reader).unwrap(),
                    "LawName" => temp_law.name = {
                        // println!("name is {:?}", e.unescape_and_decode(&reader).unwrap());
                        e.unescape_and_decode(&reader).unwrap()
                    },
                    "LawNo" => {
                        // println!("no is {:?}", e.unescape_and_decode(&reader).unwrap());
                        temp_law.no = e.unescape_and_decode(&reader).unwrap()
                    },
                    "PromulgationDate" => {
                        let date_string = e.unescape_and_decode(&reader).unwrap();
                        let datetime = DateTime::parse_from_str(&(date_string + " 00:00:00 +0900"), "%Y%m%d %H:%M:%S %z").unwrap();
                        temp_law.date = Date::build(datetime.year(), datetime.month(), datetime.day(), 0, 0, 0, 0);
                    },
                    _ => {
                        // println!("この値はスルー {:?}", current_tag_name)
                    }
                }
            },
            Ok(Event::Eof) => break, // exits the loop when reaching end of file
            Err(e) => panic!("Error at position {}: {:?}", reader.buffer_position(), e),
            _ => (), // There are several other `Event`s we do not consider here
        }

        // if we don't keep a borrow elsewhere, we can clear the buffer to keep memory usage low
        buf.clear();
    };

    law_list
}

fn get_content_by_no(no: & String, client: &reqwest::Client) -> Vec<String>{
    let url = format!("https://elaws.e-gov.go.jp/api/1/lawdata/{}", no);
    let xml_content = client.get(&url).send().unwrap().text().unwrap();

    let mut law_list = vec!();
    let mut reader = Reader::from_str(&xml_content);
    reader.trim_text(true);

    let mut buf = Vec::new();

    // The `Reader` does not implement `Iterator` because it outputs borrowed data (`Cow`s)
    let mut current_tag_name: Option<String> = None;
    loop {
        match reader.read_event(&mut buf) {
            Ok(Event::Start(ref e)) => {
                current_tag_name = Some(String::from_utf8(e.name().to_vec()).unwrap());
                // if e.name() == b"LawNameListInfo" {
                //     current_tag_name = Some(String::from_utf8(e.name().to_vec()).unwrap());
                //     println!("{:?}", current_tag_name);
                // }
            },
            // Ok(Event::End(e)) => {
            //     current_tag_name = None;
            //     if e.name() == b"LawNameListInfo" {
            //         // listに追加
            //         // println!("{:?}", temp_law);
            //         law_list.push(temp_law.clone());
            //     }
            // },
            Ok(Event::Text(e)) => {
                if current_tag_name.as_ref() == None {
                    continue;
                }

                match current_tag_name.as_ref().unwrap().as_str() {
                    // "LawName" => temp_law.name = e.unescape_and_decode(&reader).unwrap(),
                    "Sentence" => {
                        law_list.push(e.unescape_and_decode(&reader).unwrap());
                    },
                    // "LawNo" => {
                    //     // println!("no is {:?}", e.unescape_and_decode(&reader).unwrap());
                    //     temp_law.no = e.unescape_and_decode(&reader).unwrap()
                    // },
                    // "PromulgationDate" => {
                    //     let date_string = e.unescape_and_decode(&reader).unwrap();
                    //     println!("{}", &date_string);
                    //     let datetime = DateTime::parse_from_str(&(date_string + " 00:00:00 +0900"), "%Y%m%d %H:%M:%S %z").unwrap();
                    //     temp_law.date = Date::build(datetime.year(), datetime.month(), datetime.day(), 0, 0, 0, 0);
                    // },
                    _ => {
                        // println!("この値はスルー {:?}", current_tag_name)
                    }
                }
            },
            Ok(Event::Eof) => break, // exits the loop when reaching end of file
            Err(e) => panic!("Error at position {}: {:?}", reader.buffer_position(), e),
            _ => (), // There are several other `Event`s we do not consider here
        }

        // if we don't keep a borrow elsewhere, we can clear the buffer to keep memory usage low
        buf.clear();
    };

    law_list
}

fn save(list: Vec::<LawDetail>) -> Vec::<IndexResponse> {
    // A reqwest HTTP client and default parameters.
    // The builder includes the base node url (http://localhost:9200).
    let client = SyncClient::builder().build().unwrap();

    list.iter().map(|law_detail| {
        client.document()
            .index(law_detail)
            .send().expect("失敗した！！")
    }).collect()
}

// fn async_get() {
//     use futures::future::join_all;

//     // let client = reqwest::r#async::Client::builder()
//     // .timeout(Duration::from_secs(90))
//     // .build().unwrap();

//     let no_list = vec!["昭和二十六年建設省令第五号", "昭和二十六年法律第八十八号", "明治四十二年大蔵省令第二十八号"];
//     let processes = no_list.into_iter().map(|no| {
//         reqwest::r#async::Client::new().get(&format!("https://elaws.e-gov.go.jp/api/1/lawdata/{}", no))
//         .send()
//         .and_then(|_| {
//             Ok(())
//         })
//     });


//     join_all(processes.collect());

    
//     // let future1 = Future::ok::<u32, u32>(1)
//     //     .map(|x| x + 3)
//     //     .map_err(|e| println!("Error: {:?}", e))
//     //     .and_then(|x| Ok(x - 3))
//     //     .then(|res| {
//     //       match res {
//     //           Ok(val) => Ok(val + 3),
//     //           err => err,
//     //       }
//     //     });
//     // let joined_future = Future::join(future1, Future::err::<u32, u32>(2));
//     // let val = block_on(joined_future);
//     // assert_eq!(val, (Ok(4), Err(2)));
// }

// fn update(name: String) -> Vec::<IndexResponse> {
//     // A reqwest HTTP client and default parameters.
//     // The builder includes the base node url (http://localhost:9200).
//     // let client = SyncClient::builder().build().unwrap();

//     // list.iter().map(|law| {
//     //     client.document()
//     //         .update(serde_json::json!({
//     //             "sentence": "New Title"
//     //         })).expect("失敗した！！")
//     // }).collect()
// }


fn get_xml_content() -> Result<String, Box<std::error::Error>> {
    let xml_content = reqwest::get("https://elaws.e-gov.go.jp/api/1/lawlists/1")?.text()?;
    Ok(xml_content)
}
