fn main() {
    println!("Hello, world!");

    let exp = "6.1 5.2 4.3 * + 3.4 2.5 / 1.6 * -";

    let ans = rpn(exp);

    debug_assert_eq!("26.2840", format!("{:.4}", ans));
}


fn rpn(exp: &str) -> f64 {
    
    let mut stack = vec!();

    for token in exp.split_whitespace() {
        if let Ok(num) = token.parse::<f64>() {
            stack.push(num);
        } else {
            match token {
                "+" => apply2(&mut stack, |x, y|  x+y),
                "-" => apply2(&mut stack, |x, y|  x-y),
                "*" => apply2(&mut stack, |x, y|  x*y),
                "/" => apply2(&mut stack, |x, y|  x/y),
                _ => panic!("Can't parse!!")
            }
        }

        
    }
    stack.pop().expect("stack is empty")
}


fn apply2<F>(stack: &mut Vec<f64>, func: F)
where F: Fn(f64, f64)->f64 {
    if let (Some(x), Some(y)) = (stack.pop(), stack.pop()) {
        let result = func(y, x);
        stack.push(result);
    } else {
        panic!("Can't get arg.");
    }
}