#[macro_use]
extern crate log;

use actix_web::{post, web, App, HttpResponse, HttpServer, Responder};

// for Hot reloading
use listenfd::ListenFd;

use serde_derive::{Deserialize, Serialize};

// マルチプロセスシングルプロデューサー
use std::sync::mpsc;

#[derive(Default, Serialize, Deserialize, Debug, Clone)]
struct Task {
    response_url: String,
    token: String,
    text: String,
    command: String,
    user_id: String,
    user_name: String,
    channel_id: String,
}

struct AppStateProduer {
    producer: mpsc::Sender<Task>,
}

#[actix_rt::main]
async fn main() {
    env_logger::init();

    let (tx, rx) = mpsc::channel();

    let mut server = HttpServer::new(move || {
        App::new()
            .data(AppStateProduer {
                producer: tx.clone(),
            })
            .route("/", web::get().to(index))
            .service(index3)
    });
    info!("server setting end!");

    // Hot reloading
    let mut listenfd = ListenFd::from_env();
    server = if let Some(l) = listenfd.take_tcp_listener(0).unwrap() {
        server.listen(l).unwrap()
    } else {
        server.bind("0.0.0.0:8088").unwrap()
    };

    info!("server ok");

    std::thread::spawn(move || {
        for received_task in rx {
            send_request_to_time_tracking(received_task.clone());
            send_request_to_spend(received_task);
        }
    });

    server.run().unwrap();
}

#[cfg(debug_assertions)]
fn send_request_to_time_tracking(task: Task) {
    info!("send_request_to_time_tracking!!! {:?}", task);
}

#[cfg(not(debug_assertions))]
fn send_request_to_time_tracking(task: Task) {
    let client = Client::new();
    let res = client
        .post(
            &std::env::var("POST_URL_TO_TIME_TRACKING")
                .expect("CAN'T GET `POST_URL_TO_TIME_TRACKING` ENV!"),
        )
        .form(&task)
        .send();
}

#[cfg(debug_assertions)]
fn send_request_to_spend(task: Task) {
    info!("send_request_to_spend!!! {:?}", task);
}

#[cfg(not(debug_assertions))]
fn send_request_to_spend(task: Task) {
    let client = Client::new();
    if task.text.find(" #").is_none() {
        return;
    }
    let res = client
        .post(&std::env::var("POST_URL_TO_SPEND").expect("CAN'T GET `POST_URL_TO_SPEND` ENV!"))
        .form(&task)
        .send();
}

fn index() -> impl Responder {
    HttpResponse::Ok().body("Hello world!")
}

#[post("/handler")]
fn index3(task_wrapper: web::Form<Task>, state: web::Data<AppStateProduer>) -> impl Responder {
    let task = task_wrapper.into_inner();
    state.producer.send(task).expect("CAN'T SEND!!!!");
    HttpResponse::Ok().body("")
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    const BASE_URL: &str = "http://localhost:8088";

    #[actix_rt::test]
    async fn test_e2e() {
        let client = reqwest::Client::new();
        let mut params = HashMap::new();
        params.insert("response_url", "a");
        params.insert("token", "b");
        params.insert("text", "c");
        params.insert("command", "d");
        params.insert("user_id", "e");
        params.insert("user_name", "f");
        params.insert("channel_id", "g");

        let res = client
            .post(&format!("{}{}", BASE_URL, "/handler"))
            .form(&params)
            .send();

        assert!(res.is_ok());
    }
}
