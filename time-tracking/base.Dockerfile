FROM rust:1.38

WORKDIR /usr/src/myapp
COPY . .

# RUN cargo install --path .
RUN cargo build --release

CMD ["cargo", "run", "--release"]