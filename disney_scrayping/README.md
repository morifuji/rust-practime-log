#### Diesel

```
brew install mysql

cargo install diesel_cli
```

#### Mysql

```
docker run --name mysql -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=test -p 3306:3306 -d mysql:5.7
```
#### setup

```
export DATABASE_URL = mysql://yser:pass@host:3306/dbname
```

#### build

```
docker build ./ -t disney-scrayping:20191028_01
```