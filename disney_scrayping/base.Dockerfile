FROM rust:1.38

# リクエストを送るURL
ARG DATABASE_URL

COPY . .

# RUN cargo install --path .
RUN cargo build --release

CMD ["cargo", "run", "--release", "--bin", "main"]