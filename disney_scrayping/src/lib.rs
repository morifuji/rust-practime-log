pub mod schema;
pub mod models;

#[macro_use]
extern crate diesel;

// extern crate dotenv;

use self::models::{Post, NewPost};

use diesel::prelude::*;
use diesel::mysql::MysqlConnection;

pub fn establish_connection() -> MysqlConnection {

    let database_url = std::env::var("DATABASE_URL").expect("CAN'T GET `DATABASE_URL` ENV");
    MysqlConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url))
}

pub fn create_post<'a>(conn: &MysqlConnection, title: &'a str, body: &'a str) -> Post {
    use schema::posts;

    let new_post = NewPost {
        title: title,
        body: body,
    };
    let hoge = NewPost {
        title: title,
        body: body,
    };

    diesel::insert_into(posts::table)
        .values(&vec![new_post, hoge])
        .execute(conn)
        .expect("Error saving new post");

    posts::table.order(posts::id.desc()).first(conn).unwrap()
}