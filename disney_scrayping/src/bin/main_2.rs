extern crate disney_scrayping;

// use self::models::WaitingTime;
use reqwest;

use self::disney_scrayping::models::NewWaitingTime;
use self::disney_scrayping::schema::waiting_times;
use self::disney_scrayping::establish_connection;

#[macro_use]
extern crate diesel;

use diesel::prelude::*;

use scraper::{ Html, Selector };

use serde::{Serialize, Deserialize };

extern crate serde_json;

#[derive(Serialize, Deserialize)]
#[serde(untagged)]
enum InfoEnum {
    Info1  {FacilityName: String, StandbyTime: Option<String>, UpdateTime: String, StandbyTimeMax: Option<String>,StandbyTimeMin: Option<String>},
    Info2  {FacilityName: String, StandbyTime: Option<bool>},
}

#[derive(Serialize, Deserialize)]
struct InfoInGreeting {
    Facility: Vec<InfoInGreetingChild>
}

#[derive(Serialize, Deserialize)]
struct InfoInGreetingChild {
    greeting: InfoInGreetingFacility
}

#[derive(Serialize, Deserialize)]
struct InfoInGreetingFacility {
    FacilityName: String,
    StandbyTime: Option<String>,
    StandbyTimeMax: Option<String>,
    StandbyTimeMin: Option<String>,
    UpdateTime: String
}

fn main() {
    match site_handler(DisneyLandAttraction{}) {
        Some(size) => println!("get data with {:?} size", size),
        None => println!("DisneyLandAttraction can't get!!!!")
    }

    match site_handler(DisneyLandGreeting{}) {
        Some(size) => println!("get data with {:?} size", size),
        None => println!("DisneyLandGreeting can't get!!!!")
    }

    match site_handler(DisneyLandRestaurant{}) {
        Some(size) => println!("get data with {:?} size", size),
        None => println!("DisneyLandRestaurant can't get!!!!")
    }

    match site_handler(DisneySeaAttraction{}) {
        Some(size) => println!("get data with {:?} size", size),
        None => println!("DisneySeaAttraction can't get!!!!")
    }

    match site_handler(DisneySeaGreeting{}) {
        Some(size) => println!("get data with {:?} size", size),
        None => println!("DisneySeaGreeting can't get!!!!")
    }

    match site_handler(DisneySeaRestaurant{}) {
        Some(size) => println!("get data with {:?} size", size),
        None => println!("DisneySeaRestaurant can't get!!!!")
    }
}


fn site_handler<T: Site>(t: T) -> Option<QueryResult<usize>> {
    // serdeのDeserializeを持っていればそのまま `json()` で楽々呼び出せる！！
    let waiting_time_list = t.get_waiting_time_list()?;

    let connection = establish_connection();
    let result_size = diesel::insert_into(waiting_times::table)
        .values(waiting_time_list)
        .execute(&connection);

    Some(result_size)
}

pub trait Site {
  fn get_site(&self) -> &'static str;
  fn get_type(&self) -> &'static str;
  fn get_url(&self) -> &'static str;

  fn get_waiting_time_list(&self) -> Option<Vec<NewWaitingTime>> {
    let info_list_result: Vec<InfoEnum> = reqwest::get(self.get_url()).expect("CAN'T GET RESPONSE!!").json().expect("CAN'T PARSE");

    let _type = self.get_type();

    let waiting_time_list = info_list_result.iter().map(|info| {
        match info {
            InfoEnum::Info1 { StandbyTime: s, FacilityName: f, UpdateTime: u, StandbyTimeMax: s_max, StandbyTimeMin: s_min } => {
                NewWaitingTime {
                    site: self.get_site(),
                    _type: self.get_type(),
                    time_content: s.clone(),
                    facility: f.to_string(),
                    update_time: Some(u.to_string()),
                    time_content_max: s_max.clone(),
                    time_content_min: s_min.clone(),
                    ..Default::default()
                }
            },
            InfoEnum::Info2 { StandbyTime: _, FacilityName: f } => {
                NewWaitingTime {
                    site: self.get_site(),
                    _type: self.get_type(),
                    time_content: None,
                    facility: f.to_string(),
                    update_time: None,
                    ..Default::default()
                }
            }
        }
    }).collect::<Vec<NewWaitingTime>>();

    Some(waiting_time_list)
  }
}

struct DisneyLandAttraction {}
impl Site for DisneyLandAttraction {
    fn get_site(&self) -> &'static str {
        "disney_land"
    }
    fn get_type(&self) -> &'static str {
        "attraction"
    }
    fn get_url(&self) -> &'static str {
        "https://www.tokyodisneyresort.jp/_/realtime/tdl_attraction.json"
    }
}

struct DisneyLandGreeting {}
impl Site for DisneyLandGreeting {
    fn get_site(&self) -> &'static str {
        "disney_land"
    }
    fn get_type(&self) -> &'static str {
        "greeting"
    }
    fn get_url(&self) -> &'static str {
        "https://www.tokyodisneyresort.jp/_/realtime/tdl_greeting.json"
    }

    fn get_waiting_time_list(&self) -> Option<Vec<NewWaitingTime>> {
        let info_map_result: std::collections::HashMap<String, InfoInGreeting> = reqwest::get(self.get_url()).expect("CAN'T GET RESPONSE!!").json().expect("CAN'T PARSE");

        let _type = self.get_type();

        let mut waiting_time_list = vec![];

        for (_, v) in info_map_result.iter() {
            v.Facility.iter().for_each(|facility| {
                let waiting_time  = NewWaitingTime {
                        site: self.get_site(),
                        _type: self.get_type(),
                        facility: facility.greeting.FacilityName.to_string(),
                        update_time: Some(facility.greeting.UpdateTime.to_string()),
                        time_content: facility.greeting.StandbyTime.clone(),
                        time_content_max: facility.greeting.StandbyTimeMax.clone(),
                        time_content_min: facility.greeting.StandbyTimeMin.clone()
                };

                waiting_time_list.push(waiting_time);
            });
        }

        Some(waiting_time_list)
    }
}

struct DisneyLandRestaurant {}
impl Site for DisneyLandRestaurant {
    fn get_site(&self) -> &'static str {
        "disney_land"
    }
    fn get_type(&self) -> &'static str {
        "restaurant"
    }
    fn get_url(&self) -> &'static str {
        "https://www.tokyodisneyresort.jp/_/realtime/tdl_restaurant.json"
    }
}

struct DisneySeaAttraction {}
impl Site for DisneySeaAttraction {
    fn get_site(&self) -> &'static str {
        "disney_sea"
    }
    fn get_type(&self) -> &'static str {
        "attraction"
    }
    fn get_url(&self) -> &'static str {
        "https://www.tokyodisneyresort.jp/_/realtime/tds_attraction.json"
    }
}

struct DisneySeaGreeting {}
impl Site for DisneySeaGreeting {
    fn get_site(&self) -> &'static str {
        "disney_sea"
    }
    fn get_type(&self) -> &'static str {
        "greeting"
    }
    fn get_url(&self) -> &'static str {
        "https://www.tokyodisneyresort.jp/_/realtime/tds_greeting.json"
    }

    fn get_waiting_time_list(&self) -> Option<Vec<NewWaitingTime>> {
        let info_map_result: std::collections::HashMap<String, InfoInGreeting> = reqwest::get(self.get_url()).expect("CAN'T GET RESPONSE!!").json().expect("CAN'T PARSE");

        let _type = self.get_type();

        let mut waiting_time_list = vec![];

        for (_, v) in info_map_result.iter() {
            v.Facility.iter().for_each(|facility| {
                let waiting_time  = NewWaitingTime {
                        site: self.get_site(),
                        _type: self.get_type(),
                        facility: facility.greeting.FacilityName.to_string(),
                        update_time: Some(facility.greeting.UpdateTime.to_string()),
                        time_content: facility.greeting.StandbyTime.clone(),
                        time_content_max: facility.greeting.StandbyTimeMax.clone(),
                        time_content_min: facility.greeting.StandbyTimeMin.clone()
                };

                waiting_time_list.push(waiting_time);
            });
        }

        Some(waiting_time_list)
    }
}



struct DisneySeaRestaurant {}
impl Site for DisneySeaRestaurant {
    fn get_site(&self) -> &'static str {
        "disney_sea"
    }
    fn get_type(&self) -> &'static str {
        "restaurant"
    }
    fn get_url(&self) -> &'static str {
        "https://www.tokyodisneyresort.jp/_/realtime/tds_restaurant.json"
    }
}
