extern crate disney_scrayping;
extern crate diesel;

use self::diesel::prelude::*;
use self::disney_scrayping::*;
use self::models::Post;
use std::env::args;

fn main() {
    use disney_scrayping::schema::posts::dsl::{posts, published};

    let id = args().nth(1).expect("publish_post requires a post id")
        .parse::<u32>().expect("Invalid ID");
    let connection = establish_connection();

    let post = diesel::update(posts.find(id))
        .set(published.eq(true))
        .execute(&connection)
        .expect(&format!("Unable to find post {}", id));
    println!("Published post {:?}", post);
}