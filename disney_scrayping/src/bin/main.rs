extern crate disney_scrayping;

// use self::models::WaitingTime;
use reqwest;

use self::disney_scrayping::models::NewWaitingTime;
use self::disney_scrayping::schema::waiting_times;
use self::disney_scrayping::establish_connection;

#[macro_use]
extern crate diesel;

use diesel::prelude::*;

use scraper::{ Html, Selector };

fn main() {
    match site_handler(DisneyLandAttraction{}) {
        Some(size) => println!("get data with {:?} size", size),
        None => println!("DisneyLandAttraction can't get!!!!")
    }

    match site_handler(DisneyLandGreeting{}) {
        Some(size) => println!("get data with {:?} size", size),
        None => println!("DisneyLandGreeting can't get!!!!")
    }

    match site_handler(DisneyLandRestaurant{}) {
        Some(size) => println!("get data with {:?} size", size),
        None => println!("DisneyLandRestaurant can't get!!!!")
    }

    match site_handler(DisneySeaAttraction{}) {
        Some(size) => println!("get data with {:?} size", size),
        None => println!("DisneySeaAttraction can't get!!!!")
    }

    match site_handler(DisneySeaGreeting{}) {
        Some(size) => println!("get data with {:?} size", size),
        None => println!("DisneySeaGreeting can't get!!!!")
    }

    match site_handler(DisneySeaRestaurant{}) {
        Some(size) => println!("get data with {:?} size", size),
        None => println!("DisneySeaRestaurant can't get!!!!")
    }
}


fn site_handler<T: Site>(t: T) -> Option<QueryResult<usize>> {
    println!("{:?}: START", std::any::type_name::<T>());
    let mut response = reqwest::get(t.get_url()).expect("FAILED!!");
    let html = response.text().unwrap();
    let document = Html::parse_document(&html);

    let entity_list = t.get_waiting_time_list(document);
    if entity_list.is_none() {
        return None;
    }

    let connection = establish_connection();
    let result_size = diesel::insert_into(waiting_times::table)
        .values(entity_list.unwrap())
        .execute(&connection);

    Some(result_size)
}

pub trait Site {
  fn get_site(&self) -> &'static str;
  fn get_type(&self) -> &'static str;
  fn get_url(&self) -> &'static str;
  fn get_waiting_time_list<'a>(&self, doc: scraper::html::Html) -> Option<Vec<NewWaitingTime<'a>>> {

    let list_selecter = Selector::parse("li.listItem").unwrap();
    let facility_selecter = Selector::parse("h3").unwrap();
    let time_selecter = Selector::parse(".waitingBox").unwrap();

    if doc.select(&list_selecter).count() == 0 {
        return None;
    }

    Some(doc.select(&list_selecter).map(|selected_node| {

        let site = self.get_site().clone();
        let r#type = self.get_type().clone();

        let mut waiting_time_entity = NewWaitingTime {
            site: &site,
            _type: &r#type,
            ..Default::default()
        };

        waiting_time_entity.time_content = Some(selected_node.select(&time_selecter).next().unwrap().text().collect::<Vec<_>>().join("").clone());
        waiting_time_entity.facility = selected_node.select(&facility_selecter).next().unwrap().text().collect::<Vec<_>>().join("").clone();
        waiting_time_entity
    }).collect::<Vec<_>>())
  }
}

struct DisneyLandAttraction {}
impl Site for DisneyLandAttraction {
    fn get_site(&self) -> &'static str {
        "disney_land"
    }
    fn get_type(&self) -> &'static str {
        "attraction"
    }
    fn get_url(&self) -> &'static str {
        "https://www.tokyodisneyresort.jp/tdl/realtime/attraction/"
    }
}

struct DisneyLandGreeting {}
impl Site for DisneyLandGreeting {
    fn get_site(&self) -> &'static str {
        "disney_land"
    }
    fn get_type(&self) -> &'static str {
        "greeting"
    }
    fn get_url(&self) -> &'static str {
        "https://www.tokyodisneyresort.jp/tdl/realtime/greeting/"
    }
}

struct DisneyLandRestaurant {}
impl Site for DisneyLandRestaurant {
    fn get_site(&self) -> &'static str {
        "disney_land"
    }
    fn get_type(&self) -> &'static str {
        "restaurant"
    }
    fn get_url(&self) -> &'static str {
        "https://www.tokyodisneyresort.jp/tdl/realtime/restaurant/"
    }
}

struct DisneySeaAttraction {}
impl Site for DisneySeaAttraction {
    fn get_site(&self) -> &'static str {
        "disney_sea"
    }
    fn get_type(&self) -> &'static str {
        "attraction"
    }
    fn get_url(&self) -> &'static str {
        "https://www.tokyodisneyresort.jp/tds/realtime/attraction/"
    }
}

struct DisneySeaGreeting {}
impl Site for DisneySeaGreeting {
    fn get_site(&self) -> &'static str {
        "disney_sea"
    }
    fn get_type(&self) -> &'static str {
        "greeting"
    }
    fn get_url(&self) -> &'static str {
        "https://www.tokyodisneyresort.jp/tds/realtime/greeting/"
    }
}



struct DisneySeaRestaurant {}
impl Site for DisneySeaRestaurant {
    fn get_site(&self) -> &'static str {
        "disney_sea"
    }
    fn get_type(&self) -> &'static str {
        "restaurant"
    }
    fn get_url(&self) -> &'static str {
        "https://www.tokyodisneyresort.jp/tds/realtime/restaurant/"
    }
}
