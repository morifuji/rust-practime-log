use super::schema::{ posts, waiting_times };
// use diesel::types::Timestamp;
// use diesel::prelude::*;
use std::time::SystemTime;

#[derive(Queryable)]
pub struct Post {
    pub id: u32,
    pub title: String,
    pub body: String,
    pub published: bool,
}


#[derive(Insertable)]
#[table_name="posts"]
pub struct NewPost<'a> {
    pub title: &'a str,
    pub body: &'a str,
}


#[derive(Queryable, Default)]
pub struct WaitingTime {
    pub id: u32,
    pub site: String,
    pub _type: String,
    pub facility: String,
    pub time_content: String,
    pub time_content_max: String,
    pub time_content_min: String,
    pub score: u32,
    pub update_time: Option<String>,
    pub created_at: Option<SystemTime>,
    pub updated_at: Option<SystemTime>,
}

#[derive(Insertable, Default)]
#[table_name="waiting_times"]
pub struct NewWaitingTime<'a> {
    pub site: &'a str,
    pub _type: &'a str,
    pub facility: String,
    pub time_content: Option<String>,
    pub time_content_max: Option<String>,
    pub time_content_min: Option<String>,
    pub update_time: Option<String>,
}
