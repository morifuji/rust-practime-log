table! {
    posts (id) {
        id -> Unsigned<Integer>,
        title -> Varchar,
        body -> Text,
        published -> Bool,
    }
}

table! {
    waiting_times (id) {
        id -> Unsigned<Integer>,
        site -> Varchar,
        _type -> Varchar,
        facility -> Varchar,
        time_content -> Nullable<Varchar>,
        time_content_max -> Nullable<Varchar>,
        time_content_min -> Nullable<Varchar>,
        score -> Nullable<Unsigned<Integer>>,
        update_time -> Varchar,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}