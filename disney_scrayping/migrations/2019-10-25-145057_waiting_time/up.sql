CREATE TABLE `waiting_times` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `site` varchar(255) NOT NULL DEFAULT '',
  `_type` varchar(255) NOT NULL DEFAULT '',
  `facility` varchar(255) DEFAULT NULL,
  `time_content` varchar(511) DEFAULT NULL,
  `score` int(10) unsigned DEFAULT NULL,
  `update_time` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
